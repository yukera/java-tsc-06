package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.Collection;

public class ArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "display list of arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<String> arguments = serviceLocator.getCommandService().getCommandArgs();
        for (final String argument : arguments) System.out.println(argument);
    }

    private void showCommandValue(final String value) {
        if (DataUtil.isEmpty(value)) return;
        System.out.println(value);
    }

}
