package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.model.User;

public class UserShowProfileCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-show";
    }

    @Override
    public String description() {
        return "show user profile";
    }

    @Override
    public void execute() throws AbstractException {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        user.print();
    }

}
