package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-show-by-id";
    }

    @Override
    public String description() {
        return "show task by id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.print();
    }

}
