package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.enumerated.Sort;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Task> tasks;
        if (DataUtil.isEmpty(sort)) tasks = serviceLocator.getTaskService().findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(sortType.getComparator());
        }

        int index = 1;
        for (final Task task : tasks) {
            final String taskStatus = task.getStatus().getDisplayName();
            System.out.println(index + ". " + task + " (" + taskStatus + ")");
            index++;
        }
    }

}
