package com.tsc.jarinchekhina.tm.api.controller;

import com.tsc.jarinchekhina.tm.exception.system.UnknownArgumentException;
import com.tsc.jarinchekhina.tm.exception.system.UnknownCommandException;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void exit();

}
