package com.tsc.jarinchekhina.tm.api.controller;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject() throws AbstractException;

    void showProjectById() throws AbstractException;

    void showProjectByIndex() throws AbstractException;

    void showProjectByName() throws AbstractException;

    void updateProjectById() throws AbstractException;

    void updateProjectByIndex() throws AbstractException;

    void removeProjectById() throws AbstractException;

    void removeProjectByIndex() throws AbstractException;

    void removeProjectByName() throws AbstractException;

    void startProjectById() throws AbstractException;

    void startProjectByIndex() throws AbstractException;

    void startProjectByName() throws AbstractException;

    void finishProjectById() throws AbstractException;

    void finishProjectByIndex() throws AbstractException;

    void finishProjectByName() throws AbstractException;

    void changeProjectStatusById() throws AbstractException;

    void changeProjectStatusByIndex() throws AbstractException;

    void changeProjectStatusByName() throws AbstractException;

}
