package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyEmailException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    boolean isLoginExists(String login) throws EmptyLoginException;

    boolean isEmailExists(String email) throws EmptyLoginException, EmptyEmailException;

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User remove(User user);

    User findById(String id) throws EmptyIdException;

    User findByLogin(String login) throws EmptyLoginException;

    User findByEmail(String email) throws EmptyEmailException;

    User removeById(String id) throws EmptyIdException;

    User removeByLogin(String login) throws EmptyLoginException;

    User setPassword(String userId, String password) throws AbstractException;

    User updateUser(String userId, String firstName, String lastName, String middleName) throws EmptyIdException;

}
