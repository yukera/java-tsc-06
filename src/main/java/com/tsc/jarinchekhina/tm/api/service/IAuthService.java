package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.User;

public interface IAuthService {

    User getUser() throws AbstractException;

    String getUserId() throws AccessDeniedException;

    boolean isAuth();

    void logout();

    void login(String login, String password) throws AbstractException;

    void registry(String login, String password, String email) throws AbstractException;

}
